-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20 Jan 2019 pada 20.17
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpscore`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `background_process`
--

CREATE TABLE `background_process` (
  `id` varchar(36) NOT NULL,
  `last_update` datetime NOT NULL,
  `process_status` varchar(255) NOT NULL,
  `process_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `limit_transaksi`
--

CREATE TABLE `limit_transaksi` (
  `id` varchar(36) NOT NULL,
  `accumulated_transactions` decimal(19,2) NOT NULL,
  `daily_limit` decimal(19,2) NOT NULL,
  `round` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `wallet_balance` decimal(19,2) NOT NULL,
  `wallet_code` varchar(255) DEFAULT NULL,
  `id_merchant` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `limit_transaksi`
--

INSERT INTO `limit_transaksi` (`id`, `accumulated_transactions`, `daily_limit`, `round`, `tanggal`, `wallet_balance`, `wallet_code`, `id_merchant`) VALUES
('12323', '123.00', '123.00', 0, '0000-00-00', '2312.00', '12321', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `merchant`
--

CREATE TABLE `merchant` (
  `id` varchar(36) NOT NULL,
  `blocked` bit(1) NOT NULL,
  `default_ca_code` varchar(255) DEFAULT NULL,
  `is_routing` bit(1) NOT NULL,
  `limit_type` varchar(255) DEFAULT NULL,
  `merchant_code` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL,
  `merchant_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `merchant`
--

INSERT INTO `merchant` (`id`, `blocked`, `default_ca_code`, `is_routing`, `limit_type`, `merchant_code`, `merchant_name`, `merchant_type`) VALUES
('1', b'1111111111111111111111111111111', 'CA001', b'1111111111111111111111111111111', 'STOCK', 'M001', 'Merchant 001', 'TOPUP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `telkomsel_prepaid_bucket_stock`
--

CREATE TABLE `telkomsel_prepaid_bucket_stock` (
  `id` varchar(36) NOT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` varchar(255) DEFAULT NULL,
  `available` bit(1) NOT NULL,
  `bucket_code` varchar(255) DEFAULT NULL,
  `channel_code` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `id_merchant` varchar(36) DEFAULT NULL,
  `id_ca` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `telkomsel_prepaid_bucket_stock`
--

INSERT INTO `telkomsel_prepaid_bucket_stock` (`id`, `amount`, `approved_by`, `approved_date`, `available`, `bucket_code`, `channel_code`, `priority`, `product_code`, `quantity`, `round`, `update_by`, `update_date`, `id_merchant`, `id_ca`) VALUES
('2', '10000.00', 'dummy', '2018-01-01', b'1111111111111111111111111111111', 'B002', '6012', 2, 'P001', 998, 2, 'dummy', '2018-01-01 00:00:00', '1', '1'),
('3', '10000.00', 'dummy', '2018-01-01', b'1111111111111111111111111111111', 'B003', '6012', 3, 'P001', 999, 3, 'dummy', '2018-01-01 00:00:00', '1', '1'),
('4', '10000.00', 'dummy', '2018-01-01', b'1111111111111111111111111111111', 'B004', '6012', 4, 'P001', 999, 4, 'dummy', '2018-01-01 00:00:00', '1', '1'),
('5', '10000.00', 'dummy', '2018-01-01', b'1111111111111111111111111111111', 'B005', '6012', 5, 'P001', 997, 5, 'dummy', '2018-01-01 00:00:00', '1', '1'),
('9', '3.00', '3', '37-11-996', b'1111111111111111111111111111111', '879562040400674817', '3', 3, '879562040400674817', 2147483647, 2147483647, '2323', '0000-00-00 00:00:00', '1', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `telkomsel_prepaid_collecting_agent`
--

CREATE TABLE `telkomsel_prepaid_collecting_agent` (
  `id` varchar(36) NOT NULL,
  `branch_code` varchar(255) DEFAULT NULL,
  `collecting_agent_code` varchar(255) DEFAULT NULL,
  `collecting_agent_name` varchar(255) DEFAULT NULL,
  `location_code` varchar(255) DEFAULT NULL,
  `terminal` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `telkomsel_prepaid_collecting_agent`
--

INSERT INTO `telkomsel_prepaid_collecting_agent` (`id`, `branch_code`, `collecting_agent_code`, `collecting_agent_name`, `location_code`, `terminal`) VALUES
('1', 'branch1', 'CA001', 'CA Dummy', 'jakarta', 'jakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `telkomsel_prepaid_master_price`
--

CREATE TABLE `telkomsel_prepaid_master_price` (
  `id` varchar(36) NOT NULL,
  `denom` decimal(19,2) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fee` decimal(19,2) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `purchase_price` decimal(19,2) NOT NULL,
  `selling_price` decimal(19,2) NOT NULL,
  `id_ca` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `telkomsel_prepaid_message`
--

CREATE TABLE `telkomsel_prepaid_message` (
  `id` varchar(36) NOT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `biller_product_code` varchar(255) DEFAULT NULL,
  `branch_code` varchar(255) DEFAULT NULL,
  `ca_code` varchar(255) DEFAULT NULL,
  `location_code` varchar(255) DEFAULT NULL,
  `merchant_code` varchar(255) DEFAULT NULL,
  `merchant_product_code` varchar(255) DEFAULT NULL,
  `message_type` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `private_data` varchar(255) DEFAULT NULL,
  `response_code_biller` varchar(255) DEFAULT NULL,
  `response_code_channel` varchar(255) DEFAULT NULL,
  `response_desc_biller` varchar(255) DEFAULT NULL,
  `response_desc_channel` varchar(255) DEFAULT NULL,
  `terminal` varchar(255) DEFAULT NULL,
  `transaction_date_time` datetime DEFAULT NULL,
  `trx_id` varchar(255) DEFAULT NULL,
  `voucher_serial_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `telkomsel_prepaid_message`
--

INSERT INTO `telkomsel_prepaid_message` (`id`, `amount`, `biller_product_code`, `branch_code`, `ca_code`, `location_code`, `merchant_code`, `merchant_product_code`, `message_type`, `phone_number`, `private_data`, `response_code_biller`, `response_code_channel`, `response_desc_biller`, `response_desc_channel`, `terminal`, `transaction_date_time`, `trx_id`, `voucher_serial_number`) VALUES
('10111fa6-0afe-4a97-a2c3-96a3c29c1468', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:35', '561603', NULL),
('17c21338-65b6-4699-a04d-569688acb2d4', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:55', '263376', NULL),
('1d73bbf1-e106-46dc-940b-5b9c74719511', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:57', '750319', NULL),
('2b7f9aeb-718a-454e-9b8e-e01dd881a17f', '20000.00', NULL, '001', NULL, 'jakarta123', '5128', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:29:31', '530049', NULL),
('3cc520a2-eb44-4fa2-9617-37df16a1fa4b', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:34', '516887', NULL),
('51b5114d-78cf-489d-840f-79e1e93a1b38', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:20', '575053', NULL),
('767bdeaf-f72d-4458-8cbd-8dd98f16a7c4', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:25:46', '253760', NULL),
('76b573b0-54a4-4664-b6c0-42d48b7ca689', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:56', '300770', NULL),
('76c2136d-1308-4cd6-b57c-a0fefedb077e', '20000.00', NULL, '001', NULL, 'jakarta123', '5128', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:29:29', '826447', NULL),
('9c3e5e90-9f43-4c82-86a3-26b4c4d40321', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:24', '376384', NULL),
('a770129a-9dc2-489f-b83b-0190eb1068e7', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:01:26', '353000', NULL),
('ac0eb25d-17ca-409f-96f5-a2939cbd893e', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:25:45', '327861', NULL),
('bf446a60-05a1-4127-8b0a-a75db7050395', '20000.00', NULL, '001', NULL, 'jakarta123', '500289', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:28:22', '041526', NULL),
('c0e27b11-c086-4735-9b2c-6110143d9310', '20000.00', NULL, '001', NULL, 'jakarta123', '5128', '010004', 'REQUEST', '081282865666', NULL, NULL, NULL, NULL, NULL, 'jakatra', '2019-01-17 00:29:30', '727988', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `telkomsel_prepaid_transaction`
--

CREATE TABLE `telkomsel_prepaid_transaction` (
  `id` varchar(36) NOT NULL,
  `branch_code` varchar(255) DEFAULT NULL,
  `bucket_code` varchar(255) DEFAULT NULL,
  `ca_code` varchar(255) DEFAULT NULL,
  `denom` decimal(19,2) DEFAULT NULL,
  `location_code` varchar(255) DEFAULT NULL,
  `merchant_code` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `terminal` varchar(255) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `trx_id` varchar(255) DEFAULT NULL,
  `voucher_serial_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `web_menu`
--

CREATE TABLE `web_menu` (
  `id` int(11) NOT NULL,
  `judul_menu` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `is_main_menu` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `web_menu`
--

INSERT INTO `web_menu` (`id`, `judul_menu`, `link`, `icon`, `is_main_menu`) VALUES
(1, 'Home', 'Landing_page', 'home', 0),
(2, 'Config', 'javascript:void(0);', 'settings', 0),
(3, 'Routing', '#', '', 2),
(4, 'Routing Stock', 'Conf_routing_stock', '', 2),
(5, 'Translog', 'Translog', 'content_copy', 0),
(6, 'Limit Transaksi', 'Limit_trx', '', 2),
(7, 'tombol update', '', '', 101),
(8, 'Tombol Delete', '', '', 102),
(9, 'Manage User', 'Manage_usr', 'account_circle', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `web_user`
--

CREATE TABLE `web_user` (
  `id` int(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(72) NOT NULL,
  `lvl` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `web_user`
--

INSERT INTO `web_user` (`id`, `username`, `password`, `lvl`) VALUES
(1, 'ops', '$2a$08$QYMw6lUvhIJwk21phCch9eb/YPRHv4xnVEdmI1ngoVbG9blxwL8BW', 'OPS'),
(2, 'admin', '$2a$08$1NTG8ltHocd9QQ.ZZySj/.xANmn7N3T3cLq8HW51xIkUuHNTmMhCm', 'admin'),
(3, 'hoho', '$2a$08$hytx5LXwbeUB4VpycSTZnOp6.CgYwrifKEKrQ1tvk0bkI.42cpnd2', 'hoho'),
(4, 'prov01', '$2a$08$5S.hRYJrZm6nSjSvXQYpd.tnjvBm6BNj.NefHE0z2P6VzKjW6ABF6', 'provisioning');

-- --------------------------------------------------------

--
-- Struktur dari tabel `web_user_role`
--

CREATE TABLE `web_user_role` (
  `id` int(20) NOT NULL,
  `level` varchar(15) NOT NULL,
  `menu_id` int(20) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `web_user_role`
--

INSERT INTO `web_user_role` (`id`, `level`, `menu_id`, `status`) VALUES
(1, 'admin', 1, 'aktif'),
(2, 'admin', 2, 'aktif'),
(3, 'admin', 3, 'non aktif'),
(4, 'admin', 4, 'aktif'),
(5, 'admin', 5, 'aktif'),
(6, 'admin', 6, 'aktif'),
(7, 'admin', 7, 'aktif'),
(8, 'admin', 8, 'aktif'),
(9, 'provisioning', 1, 'aktif'),
(10, 'provisioning', 2, 'aktif'),
(11, 'provisioning', 4, 'aktif'),
(12, 'admin', 9, 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `background_process`
--
ALTER TABLE `background_process`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK3kw34hid9wc3ra4msw9o906a9` (`process_type`,`last_update`);

--
-- Indexes for table `limit_transaksi`
--
ALTER TABLE `limit_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKk95wfvqo0kgmru7n5aeuwtm34` (`tanggal`,`id_merchant`,`wallet_code`),
  ADD UNIQUE KEY `UK_mdqd80mkhj1anslqg3b06mwcy` (`wallet_code`),
  ADD KEY `idx_limit_transaksi` (`tanggal`,`id_merchant`),
  ADD KEY `FK4nxnxpmxkk98jkcvin8wfkp49` (`id_merchant`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telkomsel_prepaid_bucket_stock`
--
ALTER TABLE `telkomsel_prepaid_bucket_stock`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oo5jurugwvqeysf3vi7ynpd5u` (`bucket_code`),
  ADD KEY `idx_telkomsel_prepaid_bucket_stock` (`bucket_code`,`id_merchant`),
  ADD KEY `FKnn33daqlt56807v9gj2mcksiu` (`id_merchant`),
  ADD KEY `FKnpviw4cbkfae7v0k97jm3d89p` (`id_ca`);

--
-- Indexes for table `telkomsel_prepaid_collecting_agent`
--
ALTER TABLE `telkomsel_prepaid_collecting_agent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telkomsel_prepaid_master_price`
--
ALTER TABLE `telkomsel_prepaid_master_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8018y93qqa9fetovlae6i1nvq` (`id_ca`);

--
-- Indexes for table `telkomsel_prepaid_message`
--
ALTER TABLE `telkomsel_prepaid_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telkomsel_prepaid_transaction`
--
ALTER TABLE `telkomsel_prepaid_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_menu`
--
ALTER TABLE `web_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_user_role`
--
ALTER TABLE `web_user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `web_menu`
--
ALTER TABLE `web_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `limit_transaksi`
--
ALTER TABLE `limit_transaksi`
  ADD CONSTRAINT `FK4nxnxpmxkk98jkcvin8wfkp49` FOREIGN KEY (`id_merchant`) REFERENCES `merchant` (`id`);

--
-- Ketidakleluasaan untuk tabel `telkomsel_prepaid_bucket_stock`
--
ALTER TABLE `telkomsel_prepaid_bucket_stock`
  ADD CONSTRAINT `FKnn33daqlt56807v9gj2mcksiu` FOREIGN KEY (`id_merchant`) REFERENCES `merchant` (`id`),
  ADD CONSTRAINT `FKnpviw4cbkfae7v0k97jm3d89p` FOREIGN KEY (`id_ca`) REFERENCES `telkomsel_prepaid_collecting_agent` (`id`);

--
-- Ketidakleluasaan untuk tabel `telkomsel_prepaid_master_price`
--
ALTER TABLE `telkomsel_prepaid_master_price`
  ADD CONSTRAINT `FK8018y93qqa9fetovlae6i1nvq` FOREIGN KEY (`id_ca`) REFERENCES `merchant` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
