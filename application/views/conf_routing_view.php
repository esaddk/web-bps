<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>FINNET - BPS</title>
    <!-- Favicon-->
    <link rel="icon" href="asset/admin/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="asset/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="asset/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="asset/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="asset/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="asset/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="asset/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-light-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html">FINNET - BPS</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
               
                <!-- Tasks -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">account_box</i>
                        
                    </a>
                    
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i><?php echo $this->session->userdata('username')?></a></li>
                        <!-- <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li> -->
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo site_url('Login/Logout'); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                
                </li>
                <!-- #END# Tasks -->
                
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <!-- <div class="user-info">
            <div class="image">
                <img src="asset/admin/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $this->session->userdata('username')?> </div>                    
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo site_url('Landing_page'); ?>">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                    <i class="material-icons">clear</i>
                    <span>Config</span>
                </a>
                <ul class="ml-menu">
                    <li class="active">
                        <a href="<?php echo site_url('Conf_routing'); ?>">
                            <span>Routing</span>
                        </a>                                
                    </li> 
                    <li >
                        <a href="<?php echo site_url('Conf_routing_stock'); ?>">
                            <span>Routing Stock</span>
                        </a>                                
                    </li>                              
                </ul>
              </li> 
                <li>
                    <a href="<?php echo site_url('Translog'); ?>">
                        <i class="material-icons">content_copy</i>
                        <span>Translog</span>
                    </a>
                </li>
               
                
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <!-- <div class="legal">
            <div class="copyright">
                &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.5
            </div>
        </div> -->
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->       
</section>


    <section class="content">
        <div class="container-fluid">           
          <!-- Basic Examples -->
          <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>
                          Routing App
                      </h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);">Action</a></li>
                                  <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                      <div class="table-responsive">
                          <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                          <thead>
          <tr>
              <th>RID</th>
              <th>SOURCE</th>                    
              <th>DESTINATION</th>
              <th>GATEWAY</th>
              <th>UPDATE</th>
              <th>DELETE</th>

          </tr>
          </thead>
          <tbody>
          <?php for ($i = 0; $i < count($invlist); ++$i) { ?>
          <tr>
            
              <td><?php echo $invlist[$i]->RID;  ?></td>
              <td><?php echo $invlist[$i]->SRC; ?></td>
              <td><?php echo $invlist[$i]->DEST;  ?></td>
              <td><?php echo $invlist[$i]->GW; ?></td>
                
            <td>
            <a href="<?php echo site_url('Update_routing/index/'.$invlist[$i]->RID); ?>">
            <button type="button" class="btn btn-default waves-effect" data-title="Delete" data-toggle="modal">
                                    <i class="material-icons">mode_edit</i>
                                    
            </button>
            </a>
            
            </td>
            <td>        
              <button type="button" class="btn btn-default waves-effect" data-title="Delete" data-toggle="modal" data-target="#delete<?= $invlist[$i]->RID;?>">
                                    <i class="material-icons">clear</i>
              </button>
            </td>
            <!-- Modal delete-->
        <div class="modal fade" id="delete<?= $invlist[$i]->RID;?>"  tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
          <div class="modal-dialog"p>
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
              </div>
              <div class="modal-body">
                    <div class="alert alert-danger">
                    <span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?
              <?php echo $invlist[$i]->RID;?> 
        </div>
              </div>
              <div class="modal-footer ">
                  <a class="btn btn-default" role="button" href="<?php echo site_url('Conf_routing/routing_del/'.$invlist[$i]->RID); ?>">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Yes
                  </a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">
                      <span class="glyphicon glyphicon-remove"></span>No
                  </button>
              </div>
            </div> 
            </div>
            </div>

          </tr>
          <?php } ?>
         
          </tbody>
                           </table>
                      </div>
                      <div class="box-footer" style="margin-top: 15px;">
                          <a href=" <?php echo site_url('Add_routing'); ?>" class="btn bg-pink btn-lg waves-effect">Add Routing</a>
                      </div>
                  </div>
                  
              </div>
          </div>
      </div>
      <!-- #END# Basic Examples -->
                
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="asset/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="asset/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="asset/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="asset/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="asset/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="asset/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="asset/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="asset/admin/js/admin.js"></script>
    <script src="asset/admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="asset/admin/js/demo.js"></script>
</body>

</html>
