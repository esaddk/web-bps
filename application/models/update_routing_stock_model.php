<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class update_routing_stock_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     function get_routing_stock_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('telkomsel_prepaid_bucket_stock');
        return $query->row();
     }


public function update_routing_stock($data,$id)
{
    $this->db->where('id',$id);
    $result=$this->db->update('telkomsel_prepaid_bucket_stock',$data);
    return $result;

}



}