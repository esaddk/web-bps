<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class conf_routing_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     //baca data inventory dari db
     function get_list_routing()
     {

      $sql = "SELECT * FROM MROUTING ORDER BY RID ASC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;

     }
     
     public function get_routing_id($id) {
        $this->db->where('RID',$id);
        $query = $this->db->get('MROUTING');
        return $query->row();
     }
     
     public function delete_routing($id)
    {
        $this->db->where('RID',$id);
        $result=$this->db->delete('MROUTING');
        return $result;
    }
}
