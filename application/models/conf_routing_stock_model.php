<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class conf_routing_stock_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     //baca data inventory dari db
     function get_list_routing_stock()
     {

      $sql = "SELECT * FROM telkomsel_prepaid_bucket_stock ORDER BY id ASC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;

     }
     
     public function get_routing_stock_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('telkomsel_prepaid_bucket_stock');
        return $query->row();
     }
     
     public function delete_routing_stock($id)
    {
        $this->db->where('id',$id);
        $result=$this->db->delete('telkomsel_prepaid_bucket_stock');
        return $result;
    }
}
