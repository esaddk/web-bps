<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class limit_trx_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     //baca data inventory dari db
     function get_list_limit_trx()
     {

      $sql = "SELECT * FROM limit_transaksi ORDER BY id ASC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;

     }
     
     public function get_limit_trx_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('limit_transaksi
        ');
        return $query->row();
     }
     
     public function delete_limit_trx($id)
    {
        $this->db->where('id',$id);
        $result=$this->db->delete('limit_transaksi');
        return $result;
    }
}
