<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class update_limit_trx_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     function get_limit_trx_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('limit_transaksi');
        return $query->row();
     }


public function update_limit_trx($data,$id)
{
    $this->db->where('id',$id);
    $result=$this->db->update('limit_transaksi',$data);
    return $result;

}



}