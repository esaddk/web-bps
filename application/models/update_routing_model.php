<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class update_routing_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     function get_routing_id($id) {
        $this->db->where('RID',$id);
        $query = $this->db->get('MROUTING');
        return $query->row();
     }


public function update_routing($data,$id)
{
    $this->db->where('RID',$id);
    $result=$this->db->update('MROUTING',$data);
    return $result;

}



}