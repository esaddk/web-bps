<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class translog_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     //baca data inventory dari db
     function get_list_translog()
     {

      $sql = "SELECT * FROM telkomsel_prepaid_transaction ORDER BY id ASC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;

     }
     
     public function get_list_translog_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('telkomsel_prepaid_transaction');
        return $query->row();
     }
     
    //  public function delete_routing_stock($id)
    // {
    //     $this->db->where('RSID',$id);
    //     $result=$this->db->delete('MROUTESTOCK');
    //     return $result;
    // }
}
