<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class manage_user_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     //baca data inventory dari db
     function get_user_list()
     {

      $sql = "SELECT * FROM web_user ORDER BY id ASC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;

     }
     
     public function get_user_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('web_user');
        return $query->row();
     }
     
     public function delete_user($id)
    {
        $this->db->where('id',$id);
        $result=$this->db->delete('web_user');
        return $result;
    }
}
