<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class update_user_model extends CI_Model{
     function __construct()
     {
          //panggil model konstruktor
          parent::__construct();
     }

     function get_user_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('web_user');
        return $query->row();
     }


public function update_user($data,$id)
{
    $this->db->where('id',$id);
    $result=$this->db->update('web_user',$data);
    return $result;

}



}