<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Landing_page extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->is_logged_in(); //cek session

     }

     public function index()
     {


            //load landing_page_view
          $this->load->view('landing_page_view');

     }


      public function oracle()
     {
          redirect('Dtfraud');
     }


    //Cek Session
      function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }


}?>
