<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Limit_trx extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->is_logged_in(); //cek session
          $this->load->helper('date');
          date_default_timezone_set('Asia/Jakarta');
     }

     public function index()
     {          
          $this->load->model('limit_trx_model');          
          $invresult = $this->limit_trx_model->get_list_limit_trx();
          $data['invlist'] = $invresult;          
          $this->load->view('limit_trx_view',$data);
     }
     
     public function limit_trx_del($id)
     {
          $this->load->model('limit_trx_model');
          $result=$this->limit_trx_model->get_limit_trx_id($id);
          $result =$this->limit_trx_model->delete_limit_trx($id);
          if ($result == 1) {
          echo "<script> alert('DATA DELETED') </script>";
           } else {
             echo "<script> alert('ERROR CONSTRAINT') </script>";
           }
          redirect('Limit_trx','refresh');           
     }
     
     //Cek Session
     function is_logged_in()
     {
         $is_logged_in = $this->session->userdata('is_logged_in');
         if(!isset($is_logged_in) ||  $is_logged_in != true)
          {
               redirect('Login');
          }
     }


         

}?>
