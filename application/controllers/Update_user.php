<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Update_user extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->load->library('bcrypt');
          $this->load->model('update_user_model');
          $this->load->model('insert_user_model');
          $this->is_logged_in(); //cek session

     }

     public function index($id)
     {
          //load oracle_model 

          $result=$this->update_user_model->get_user_id($id);
          $data['id'] = $result->id;
          $data['user'] = $result->username;
          $data['password'] = $result->password;
          $data['lvl'] = $result->lvl;
          //print_r($data);
          $this->load->view('update_user_view',$data);
     }

     public function update()
     {
       $id   = $this->input->post('id');
       $password = $this->input->post('password');
       $hash = $this->bcrypt->hash_password($password);
       
       $data = array(

           'id'                 => $this->input->post('id'),
           'username'           => $this->input->post('username'),
           'password'           => $hash,
           'lvl'                => $this->input->post('lvl')
          
            );
       $result =$this->update_user_model->update_user($data,$id);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }

       redirect('Manage_usr','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
