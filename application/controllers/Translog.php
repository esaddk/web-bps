<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Translog extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->is_logged_in(); //cek session
          $this->load->helper('date');
          date_default_timezone_set('Asia/Jakarta');
     }

     public function index()
     {          
          $this->load->model('translog_model');          
          $invresult = $this->translog_model->get_list_translog();
          $data['invlist'] = $invresult;
          //load oracle_view
          //print_r ($data);
          $this->load->view('translog_view',$data);
     }
     
    //  public function routing_stock_del($id)
    //  {
    //       $this->load->model('conf_routing_stock_model');
    //       $result=$this->conf_routing_stock_model->get_routing_stock_id($id);
    //       $result =$this->conf_routing_stock_model->delete_routing_stock($id);
    //       if ($result == 1) {
    //       echo "<script> alert('DATA DELETED') </script>";
    //        } else {
    //          echo "<script> alert('ERROR CONSTRAINT') </script>";
    //        }
    //       redirect('Conf_routing_stock','refresh');           
    //  }
     
     //Cek Session
     function is_logged_in()
     {
         $is_logged_in = $this->session->userdata('is_logged_in');
         if(!isset($is_logged_in) ||  $is_logged_in != true)
          {
               redirect('Login');
          }
     }


         

}?>
