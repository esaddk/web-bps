<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Update_routing extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->load->model('update_routing_model');
          $this->load->model('add_routing_model');
          $this->is_logged_in(); //cek session

     }

     public function index($id)
     {
          //load oracle_model 

          $result=$this->update_routing_model->get_routing_id($id);
          $data['RID'] = $result->RID;
          $data['SRC'] = $result->SRC;
          $data['DEST'] = $result->DEST;
          $data['GW'] = $result->GW;
          //print_r($data);
          $this->load->view('update_routing_view',$data);
     }

     public function update()
     {
       $id   = $this->input->post('RID');
       $data = array(

           'RID'                  => $this->input->post('RID'),
           'SRC'                  => $this->input->post('SRC'),           
           'DEST'                 => $this->input->post('DEST'),
           'GW'                   => $this->input->post('GW'),
          
            );
       $result =$this->update_routing_model->update_routing($data,$id);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }

       redirect('Conf_routing','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
