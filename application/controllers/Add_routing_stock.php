<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Add_routing_stock extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');          
          $this->load->database();
          $this->load->model('add_routing_stock_model');
          $this->is_logged_in(); //cek session

     }

     public function index()
     {          
          $seq_user_id = $this->add_routing_stock_model->get_routing_stock_id();
          $data['seq_user_id']= $seq_user_id;         
          $this->load->view('add_routing_stock_view',$data);
     }

     

     public function add_routing_stock()
     {     

       $data = array(
        'id'                                   => $this->input->post('id'),
        'amount'                               => $this->input->post('amount'),           
        'approved_by'                          => $this->input->post('approved_by'),
        'approved_date'                        => $this->input->post('approved_date'),
        'available'                            => $this->input->post('available'),   
        'bucket_code'                          => $this->input->post('bucket_code'),        
        'channel_code'                         => $this->input->post('channel_code'),           
        'id_ca'                                => $this->input->post('id_ca'),
        'id_merchant'                          => $this->input->post('id_merchant'),
        'priority'                             => $this->input->post('priority'),
        'product_code'                         => $this->input->post('product_code'),
        'quantity'                             => $this->input->post('quantity'),
        'round'                                => $this->input->post('round'),
        'update_by'                            => $this->input->post('update_by'),
        'update_date'                          => $this->input->post('update_date') 
            );

       $result =$this->add_routing_stock_model->add_routing_stock($data);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }
       redirect('Conf_routing_stock','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
