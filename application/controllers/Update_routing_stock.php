<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Update_routing_stock extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->load->model('update_routing_stock_model');
          $this->load->model('add_routing_stock_model');
          $this->is_logged_in(); //cek session

     }

     public function index($id)
     {
          //load oracle_model 

          $result=$this->update_routing_stock_model->get_routing_stock_id($id);
          $data['id'] = $result->id;
          $data['amount'] = $result->amount;
          $data['approved_by'] = $result->approved_by;
          $data['approved_date'] = $result->approved_date;
          $data['available'] = $result->available;          
          $data['bucket_code'] = $result->available;          
          $data['channel_code'] = $result->channel_code;
          $data['id_ca'] = $result->id_ca;
          $data['id_merchant'] = $result->id_merchant;
          $data['priority'] = $result->priority;
          $data['product_code'] = $result->available;
          $data['quantity'] = $result->available;
          $data['round'] = $result->available;
          $data['update_by'] = $result->update_by;
          $data['update_date'] = $result->update_date;
          //print_r($data);
          $this->load->view('update_routing_stock_view',$data);
     }

     public function update()
     {
       $id   = $this->input->post('id');
       $data = array(

           'id'                                   => $this->input->post('id'),
           'amount'                               => $this->input->post('amount'),           
           'approved_by'                          => $this->input->post('approved_by'),
           'approved_date'                        => $this->input->post('approved_date'),
           'available'                            => $this->input->post('available'),   
           'bucket_code'                          => $this->input->post('bucket_code'),        
           'channel_code'                         => $this->input->post('channel_code'),           
           'id_ca'                                => $this->input->post('id_ca'),
           'id_merchant'                          => $this->input->post('id_merchant'),
           'priority'                             => $this->input->post('priority'),
           'product_code'                         => $this->input->post('product_code'),
           'quantity'                             => $this->input->post('quantity'),
           'round'                                => $this->input->post('round'),
           'update_by'                            => $this->input->post('update_by'),
           'update_date'                          => $this->input->post('update_date')           
           
          
            );
       $result =$this->update_routing_stock_model->update_routing_stock($data,$id);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }

       redirect('Conf_routing_stock','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
