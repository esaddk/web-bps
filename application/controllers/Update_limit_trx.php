<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Update_limit_trx extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->load->model('update_limit_trx_model');
          $this->load->model('limit_trx_model');
          $this->is_logged_in(); //cek session

     }

     public function index($id)
     {
          //load oracle_model 

          $result=$this->update_limit_trx_model->get_limit_trx_id($id);
          $data['id'] = $result->id;
          $data['accumulated_transactions'] = $result->accumulated_transactions;
          $data['daily_limit'] = $result->daily_limit;
          $data['round'] = $result->round;
          $data['tanggal'] = $result->tanggal;          
          $data['wallet_balance'] = $result->wallet_balance;          
          $data['wallet_code'] = $result->wallet_code;
          $data['id_merchant'] = $result->id_merchant;
          
          //print_r($data);
          $this->load->view('update_limit_trx_view',$data);
     }

     public function update()
     {
       $id   = $this->input->post('id');
       $data = array(

           'id'                                   => $this->input->post('id'),
           'accumulated_transactions'             => $this->input->post('accumulated_transactions'),           
           'daily_limit'                          => $this->input->post('daily_limit'),
           'round'                                => $this->input->post('round'),
           'tanggal'                              => $this->input->post('tanggal'),   
           'wallet_balance'                       => $this->input->post('wallet_balance'),        
           'wallet_code'                          => $this->input->post('wallet_code'),           
           'id_merchant'                          => $this->input->post('id_merchant')                                       
            );
       $result =$this->update_limit_trx_model->update_limit_trx($data,$id);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }

       redirect('Limit_trx','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
