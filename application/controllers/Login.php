<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class Login extends CI_Controller
{

     public function __construct()
     {
          parent::__construct();
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          $this->load->library('bcrypt');
          //ambil login model
          $this->load->model('login_model');
     }

     public function index()
     {
          //ambil nilai post dari input username di view
          $username = $this->input->post("txt_username");
          $password = $this->input->post("txt_password");

          //validasi
          $this->form_validation->set_rules("txt_username", "Username", "trim|required");
          $this->form_validation->set_rules("txt_password", "Password", "trim|required");

          if ($this->form_validation->run() == FALSE)
          {
               //vaslidasi salah
               $this->load->view('login_view');
          }
          else
          {
               //validasi sukses
               if ($this->input->post('btn_login') == "Login")
               {
                    //cek username dan password benar
                    $usr_result = $this->login_model->get_user($username);
                    
                    
                    
                    if ($usr_result != FALSE) //menhasilkan aktiv user record
                    {
                         //set sesion variabel
                         $sessiondata = array(
                              'id' => $usr_result[0]->id,
                              'username' => $usr_result[0]->username,
                              'role' => $usr_result[0]->lvl,
                              'loginuser' => TRUE,
                              'is_logged_in' => true
                              
                              
                         );
                         $this->session->set_userdata($sessiondata);
                         //$this->load->view('inventory_view');  jika
                         //redirect('Landing_page');
                         //print_r($usr_result);
                         //echo $this->session->userdata('role');
                         

                          if ($this->bcrypt->check_password($password, $usr_result[0]->password))
                    {
                             redirect('Landing_page');
                    }
                    else
                    {
                            $this->session->set_flashdata('danger', 'Usename / Password Salah HOH');
                            redirect('Login');
                    }
                    
                         
                    }                                      
                    else
                    {
                         $this->session->set_flashdata('danger', 'Usename / Password Salah HIHI');
                         redirect('Login');
                    }
               }
               else
               {
                    redirect('Login');
               }
          }
     }

     //Logout
      function logout()
      {
          $user_data = $this->session->all_userdata();
              foreach ($user_data as $key => $value) {
                  if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                      $this->session->unset_userdata($key);
                  }
              }
          $this->session->sess_destroy();
          redirect('Login', 'refresh');
      }
}?>
