<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Add_routing extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->library('bcrypt');
          $this->load->database();
          $this->load->model('add_routing_model');
          $this->is_logged_in(); //cek session

     }

     public function index()
     {          
          $seq_user_id = $this->add_routing_model->get_routing_id();
          $data['seq_user_id']= $seq_user_id;         
          $this->load->view('add_routing_view',$data);
     }

     

     public function add_routing()
     {     

       $data = array(
           'RID'                  => $this->input->post('RID'),
           'SRC'                  => $this->input->post('SRC'),           
           'DEST'                 => $this->input->post('DEST'),
           'GW'                   => $this->input->post('GW'),
            );

       $result =$this->add_routing_model->add_routing($data);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }
       redirect('Conf_routing','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
