<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Manage_usr extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->is_logged_in(); //cek session
          $this->load->helper('date');
          date_default_timezone_set('Asia/Jakarta');
     }

     public function index()
     {
          //load manage user model
          $this->load->model('manage_user_model');
          //Memanggil model function untuk ambil data oracle
          $invresult = $this->manage_user_model->get_user_list();
          $data['invlist'] = $invresult;
          //load oracle_view
          //print_r ($data);
          $this->load->view('manage_user_view',$data);
     }
     
     public function m_user_del($id)
     {
          $this->load->model('manage_user_model');
          $result=$this->manage_user_model->get_user_id($id);
          $result =$this->manage_user_model->delete_user($id);
          if ($result == 1) {
          echo "<script> alert('DATA DELETED') </script>";
           } else {
             echo "<script> alert('ERROR CONSTRAINT') </script>";
           }
          redirect('Manage_usr','refresh');           
     }
     
     //Cek Session
     function is_logged_in()
     {
         $is_logged_in = $this->session->userdata('is_logged_in');
         if(!isset($is_logged_in) ||  $is_logged_in != true)
          {
               redirect('Login');
          }
     }


         

}?>
