<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Add_limit_trx extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');          
          $this->load->database();
          $this->load->model('add_limit_trx_model');
          $this->is_logged_in(); //cek session

     }

     public function index()
     {          
          $seq_user_id = $this->add_limit_trx_model->get_limit_trx_id();
          $data['seq_user_id']= $seq_user_id;         
          $this->load->view('add_limit_trx_view',$data);
     }

     

     public function add_limit_trx()
     {     

       $data = array(
        'id'                                   => $this->input->post('id'),
        'accumulated_transactions'             => $this->input->post('accumulated_transactions'),           
        'daily_limit'                          => $this->input->post('daily_limit'),
        'round'                                => $this->input->post('round'),
        'tanggal'                              => $this->input->post('tanggal'),   
        'wallet_balance'                       => $this->input->post('wallet_balance'),        
        'wallet_code'                          => $this->input->post('wallet_code'),           
        'id_merchant'                          => $this->input->post('id_merchant') 
            );

       $result =$this->add_limit_trx_model->add_limit_trx($data);
       if ($result == 1) {
         echo "<script> alert('sukses') </script>";

       } else {
         echo "<script> alert('gagal') </script>";
       }
       redirect('Limit_trx','refresh');

     }

     //Cek Session
function is_logged_in()
    {
    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) ||  $is_logged_in != true)
    {

          redirect('Login');

    }
    }

}?>
