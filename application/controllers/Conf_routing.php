<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Conf_routing extends CI_Controller {
     public function __construct()
     {
          parent::__construct();
          $this->load->helper('url');
          $this->load->database();
          $this->is_logged_in(); //cek session
          $this->load->helper('date');
          date_default_timezone_set('Asia/Jakarta');
     }

     public function index()
     {
          //load manage user model
          $this->load->model('conf_routing_model');
          //Memanggil model function untuk ambil data oracle
          $invresult = $this->conf_routing_model->get_list_routing();
          $data['invlist'] = $invresult;
          //load oracle_view
          //print_r ($data);
          $this->load->view('conf_routing_view',$data);
     }
     
     public function routing_del($id)
     {
          $this->load->model('conf_routing_model');
          $result=$this->conf_routing_model->get_routing_id($id);
          $result =$this->conf_routing_model->delete_routing($id);
          if ($result == 1) {
          echo "<script> alert('DATA DELETED') </script>";
           } else {
             echo "<script> alert('ERROR CONSTRAINT') </script>";
           }
          redirect('Conf_routing','refresh');           
     }
     
     //Cek Session
     function is_logged_in()
     {
         $is_logged_in = $this->session->userdata('is_logged_in');
         if(!isset($is_logged_in) ||  $is_logged_in != true)
          {
               redirect('Login');
          }
     }


         

}?>
